<?php
    header('Content-Type: text/html; charset=UTF-8');    
    
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        // Массив для временного хранения сообщений пользователю.
        $messages = array();

        // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
        // Выдаем сообщение об успешном сохранении.
        if (!empty($_COOKIE['save'])) {
            // Удаляем куку, указывая время устаревания в прошлом.
            setcookie('save', '', 100000);
            // Если есть параметр save, то выводим сообщение пользователю.
            $messages[] = 'Спасибо, результаты сохранены.';
        }
        if (!empty($_COOKIE['notsave'])) {
            setcookie('notsave', '', 100000);
            $messages[] = 'Ошибка отправления в базу данных.';
        }
        


        // Складываем признак ошибок в массив.
        $errors = array();
        $errors['name'] = empty($_COOKIE['name_error']) ? '' : $_COOKIE['name_error'];
        $errors['email'] = !empty($_COOKIE['email_error']);
        $errors['superpowers'] = !empty($_COOKIE['superpowers_error']);
        $errors['bio'] = !empty($_COOKIE['bio_error']);
        $errors['checkbox'] = !empty($_COOKIE['checkbox_error']);



        // Выдаем сообщения об ошибках.
        //Ошибка имени
        if ($errors['name']=='null') {
            // Удаляем куку, указывая время устаревания в прошлом.
            setcookie('name_error', '', 100000);
            // Выводим сообщение.
            $messages[] = '<div class="error">Заполните имя.</div>';
        }
        else if ($errors['name']=='Unacceptable symbols') {
            // Удаляем куку, указывая время устаревания в прошлом.
            setcookie('name_error', '', 100000);
            // Выводим сообщение.
            $messages[] = '<div>Недопустимые символы. Введите имя заново.</div>';
        }
       
        // Ошибка Email.
        if ($errors['email']) {
            // Удаляем куку, указывая время устаревания в прошлом.
            setcookie('email_error', '', 100000);
            // Выводим сообщение.
            $messages[] = '<div class="error">Заполните почту.</div>';
        }
       
        // Ошибка суперсил.
        if ($errors['superpowers']) {
            // Удаляем куку, указывая время устаревания в прошлом.
            setcookie('superpowers_error', '', 100000);
            // Выводим сообщение.
            $messages[] = '<div>Выберите хотя бы одну сверхспособность.</div>';
        }

        // Ошибка биографии.
        if ($errors['bio']) {
            // Удаляем куку, указывая время устаревания в прошлом.
            setcookie('bio_error', '', 100000);
            // Выводим сообщение.
            $messages[] = '<div>Напишите что-нибудь о себе.</div>';
        }

        // Ошибка чекбокса.
        if ($errors['checkbox']=='null') {
            // Удаляем куку, указывая время устаревания в прошлом.
            setcookie('checkbox_error', '', 100000);
            // Выводим сообщение.
            $messages[] = '<div>Вы не можете отправить форму не согласившись с контрактом.</div>';
        }



        // Складываем предыдущие значения полей в массив, если есть.
        $values = array();
        $sila = array();
        $sila ['immortal'] = "Бессмертие";
        $sila ['walk'] = "Хождение сквозь стены";
        $sila ['polet'] = "Левитация";
        $sila ['telekinez'] = "Телекинез";
        $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
        $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
        $values['birthday'] = empty($_COOKIE['birthday_value']) ? '' : $_COOKIE['birthday_value'];
        $values['sex'] = empty($_COOKIE['sex_value']) ? 'male' : $_COOKIE['sex_value'];
        $values['limb'] = empty($_COOKIE['limb_value']) ? '4' : $_COOKIE['limb_value'];
        $values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
        
        if (!empty($_COOKIE['superpowers_value'])) {
            $superpowers_value = json_decode($_COOKIE['superpowers_value']);
        }
        $values['sila'] = [];
        if (isset($superpowers_value) && is_array($superpowers_value)) {
            foreach ($superpowers_value as $power) {
                if (!empty($sila [$power])) {
                    $values['sila'][$power] = $power;
                }
            }
        }


        // Включаем содержимое файла form.php.
        // В нем будут доступны переменные $messages, $errors и $values для вывода 
        // сообщений, полей с ранее заполненными данными и признаками ошибок.
        include('form.php');
    }



    // Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
    else {
        
        // Проверяем ошибки.
        $errors = FALSE;
        if (empty($_POST['name'])) {

            // Выдаем куку на день с флажком об ошибке в поле name.
            setcookie('name_error', 'null', time() + 24 * 60 * 60);
            $errors = TRUE;
        }
        else if (!preg_match('/^([а-яё\s]+|[a-z\s]+)$/iu', $_POST["name"])) {
            setcookie('name_error', 'Unacceptable symbols', time() + 24 * 60 * 60);
            $errors = TRUE;
        }
        else {
            // Сохраняем ранее введенное в форму значение на месяц.
            setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
        }


        if (empty($_POST['email'])) {
            setcookie('email_error', '1', time() + 24 * 60 * 60);
            $errors = TRUE;
        }
        else {
            setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
        }



        $sila = array();
        foreach ($_POST['superpowers'] as $key => $value) {
              $sila[$key] = $value;
        }
        if (!sizeof($sila)) {
            setcookie('superpowers_error', '1', time() + 24 * 60 * 60);
            $errors = TRUE;
        }
        else {
            setcookie('superpowers_value', json_encode($sila), time() + 30 * 24 * 60 * 60);
        }
         


        if (empty($_POST['bio'])) {
            // Выдаем куку на день с флажком об ошибке в поле name.
            setcookie('bio_error', '1', time() + 24 * 60 * 60);
            $errors = TRUE;
        }
        else {
            // Сохраняем ранее введенное в форму значение на месяц.
            setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60);
        }
        


        if (empty($_POST['checkbox'])) {
            // Выдаем куку на день с флажком об ошибке в поле name.
            setcookie('checkbox_error', '1', time() + 24 * 60 * 60);
            $errors = TRUE;
        }

          setcookie('birthday_value', $_POST['birthday'], time() + 30 * 24 * 60 * 60);
          setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
          setcookie('limb_value', $_POST['limb'], time() + 30 * 24 * 60 * 60);


        if ($errors) {
            // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
            header('Location: index.php');
            exit();
        }
        else {
            // Удаляем Cookies с признаками ошибок.
            setcookie('name_error', '', 100000);
            setcookie('email_error', '', 100000);
            setcookie('superpowers_error', '', 100000);
            setcookie('bio_error', '', 100000);
            setcookie('checkbox_error', '', 100000);
        }



        // Параметры для подключения
        $db_user = 'u17333'; // Логин БД
        $db_password = '7038049'; // Пароль БД
        $db_table = 'auth'; // Имя Таблицы БД
        $charset = 'utf8'; // кодировка
        $connection = 'mysql:host=localhost;dbname=u17333;cahrset=$charset';

        $name = $_POST['name'];
        $email = $_POST['email'];
        $birthday = $_POST['birthday'];
        $sex = $_POST['sex'];
        $limb = $_POST['limb'];
        $bio = $_POST['bio'];
        $checkbox = $_POST['checkbox'];
        $sila_mass = array();
        foreach ($_POST['superpowers'] as $key => $value) {
            $sila_mass[$key] = $value;
        }
        $superpowers_line = implode(', ', $sila_mass);



        try {
    
             // Подключение к базе данных
        $pdo = new PDO('mysql:host=localhost;dbname=u17333', $db_user, $db_password, array(PDO::ATTR_PERSISTENT => true));

        // Создаем запрос в базу данных и записываем его в переменную
        $statement = $pdo->prepare("INSERT INTO ".$db_table." (name, email, birthday, sex, limb, superpowers, bio) VALUES ('$name','$email',$birthday,'$sex',$limb,'$superpowers_line','$bio')");

        $statement = $pdo->prepare('INSERT INTO '.$db_table.' (name, email, birthday, sex, limb, superpowers, bio) VALUES (:name, :email, :birthday, :sex, :limb, :superpowers, :bio)');

        $result=$statement->execute([
            'name' => $name,
            'email' => $email,
            'birthday' => $birthday,
            'sex' => $sex,
            'limb' => $limb,
            'superpowers' => $superpowers_line,
            'bio' => $bio
        ]);


            // Сохраняем куку с признаком успешного сохранения.
            setcookie('save', '1');
        } catch (PDOException $e) {
            setcookie('notsave', '1');
        }

          // Делаем перенаправление.
        header('Location: index.php');
    }
?>